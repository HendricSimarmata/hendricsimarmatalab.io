<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="keywords"  content="graphic designer, ui, ux, prabumulih, indonesia, south sumatera, sumatera, logo, illustration, web, website, homepage, site, grafik, grafis, desain, desainer, illustrator, freelance, designer, hendric simarmata, hendric, simarmata, company, header, images, branding, rebranding, online, graphic design, freelancer, freelance, business, creating illustration, modify illustration, corporate identity, visual design, visual, concept, freelance web design, mobile design, ios app, android app, interface, developer, create illusration, element, ecommerce, isometric, isometric creator, creator, branding, design your own illustration, design your illustration, vector design, vector, 3d design, vector 3d, 3d, web page, parna, pekanbaru, riau, perawang, siak, siak sri indrapura, perawang siak, pekanbaru kota, reference, template design, templates, desain template, page elements, web kit, mobile kit, kits" />
<meta name="description" content="Graphic Designer specializing in illustration, ui/ux, icon, &amp; Illustrator based in Prabumulih, Indonesia.">
<meta name="author" content="Hendric Simarmata">
<meta name="google-site-verification" content="IC3j7IwOIqOGVQTuXPEeoLflvlcxv2eMxvHEsxoz-Wk" />

<title>Hendric Simarmata | Hendric Simarmata</title>
	
<!-- Favicons -->
<link rel="icon" href="apple-touch-icon-128x128.png" sizes="32x32">
<link rel="icon" href="apple-touch-icon-128x128.png" sizes="192x192">
<link rel="apple-touch-icon" href="apple-touch-icon-128x128.png">
<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">

<!-- Styles -->
<link rel="stylesheet" href="css/fontawesome/all.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:500,600,700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i&display=swap" rel="stylesheet">
<link href="css/style.css" rel="stylesheet" media="screen">
<!--<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css">-->
	
<!-- SEO -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PFGT7C3');</script>
<!-- End Google Tag Manager -->
	
<script src="https://www.googleoptimize.com/optimize.js?id=GTM-P365WGK"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-53681345-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-53681345-4');
</script>

<!-- DATA -->
<meta property="og:type" content="website">
<meta property="og:url" content="http://hendricsimarmata.com/">
<meta property="og:title" content="Hendric Simarmata | Hendric Simarmata">
<meta property="og:image" content="https://hendricsimarmata.github.io/apple-touch-icon.png">
<meta property="og:description" content="Graphic Designer specializing in illustration, ui/ux, icon, &amp; Illustrator based in Prabumulih, Indonesia.">
<meta property="og:locale" content="en-IN">

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@HendricMarmata" />
<meta name="twitter:creator" content="@HendricMarmata" />
<meta name="twitter:description" content="Graphic Designer specializing in illustration, ui/ux, icon, &amp; Illustrator based in Prabumulih, Indonesia.">
<meta name="twitter:title" content="Hendric Simarmata">
<meta name="twitter:image" content="https://hendricsimarmata.github.io/apple-touch-icon.png" />
<!-- /SEO -->
	
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PFGT7C3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	
  <div class="animsition">   
    <div class="loader"><div class="spinner"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>

    <!-- Content CLick Capture-->

    <div class="click-capture"></div>

    <!-- Sidebar Menu-->
    <div class="menu"> 
      <span class="close-menu icon-cross2 right-boxed"></span>
      <ul class="menu-list right-boxed">
        <li  data-menuanchor="page1">
          <a  href="#page1">Home</a>
        </li>
        <li  data-menuanchor="page2">
          <a href="#page2">Specialization</a>
        </li>
        <li  data-menuanchor="page3">
          <a href="#page3">Resume</a>
        </li>
        <li  data-menuanchor="page4">
          <a href="#page4">About</a>
        </li>
        <li  data-menuanchor="page5">
          <a href="#page5">Projects</a>
        </li>
        <li  data-menuanchor="page6">
          <a href="#page6">Gallery</a>
        </li>
		
        <li  data-menuanchor="page7">
          <a href="#page7">Contact</a>
        </li>
      </ul>
      <div class="menu-footer right-boxed">
        <div class="social-list">
          <a href="https://twitter.com/HendricMarmata" target="_blank" class="icon fa-xs fab fa-twitter"></a>
          <a href="https://facebook.com/HendricSimarmata" target="_blank" class="icon fab fa-facebook-f"></a>
          <a href="https://behance.net/HendricSimarmata" target="_blank" class="icon fab fa-behance"></a>
          <a href="https://linkedin.com/in/HendricSimarmata" target="_blank" class="icon fab fa-linkedin-in"></a>
          <a href="https://dribbble.com/HendricSimarmata" target="_blank" class="icon fab fa-dribbble"></a>
        </div>
        <div class="copy">&copy; Hendric Simarmata <script>document.write(new Date().getFullYear());</script>. All Rights Reserved</div>
      </div>
    </div>

    <!-- Navbar -->

    <header class="navbar navbar-fullpage boxed">
      <div class="navbar-bg"></div>
      <a class="brand" href="index.html">
        <img alt="" src="images/brand.png">
        <div class="brand-info">
          <div class="brand-name">Hendric Simarmata</div>
        </div>
      </a>

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

    </header>
    <div class="copy-bottom white boxed">&copy; Hendric Simarmata <script>document.write(new Date().getFullYear());</script>.</div>
    <div class="social-list social-list-bottom boxed">
        		<a href="https://behance.net/HendricSimarmata" target="_blank" class="icon fab fa-behance"></a>
          		<a href="https://facebook.com/HendricSimarmata" target="_blank" class="icon fab fa-facebook-f"></a>
          		<a href="https://linkedin.com/in/HendricSimarmata" target="_blank" class="icon ion-social-linkedin"></a>
          		<a href="https://dribbble.com/HendricSimarmata" target="_blank" class="icon fab fa-dribbble"></a>
      </div>
    <div class="pagepiling">
      <div data-anchor="page1" class="pp-scrollable text-white section section-1">
        <div class="scroll-wrap">
          <div class="section-bg" style="background-image:url(images/bg/main.jpg);"></div>
          <div class="scrollable-content">
            <div class="vertical-centred">
              <div class="boxed boxed-inner">
                <div class="boxed">
                  <div class="container">
                    <div class="intro">
                      <div class="row">
                        <div class="col-md-8 col-lg-6">
                          <h1 class="display-2 text-white  wow fadeIn" data-wow-delay="0.1s"><h2><span class="text-primary">Hello,</span> I'm Hendric Simarmata</h2></h1>
                          <a href="#page5" class="projectdepan"><i class="icon pelet fas fa-project-diagram"></i>See my recent Project </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div data-anchor="page2" class="pp-scrollable section section-2">
        <div class="scroll-wrap">
          <div class="scrollable-content">
            <div class="vertical-title text-white  d-none d-lg-block"><span>what I do</span></div>
            <div class="vertical-centred">
              <div class="boxed boxed-inner">
                <div class="boxed">
                  <div class="container">
                    <div class="intro">
                      <h2 class="title mb-5 pb-5"> <span class="text-primary">My</span> specialization</h2>
                      <div class="row">
                        <div class="col-md-6 col-lg-4">
                          <span class="icon-special fal fa-wand-magic"></span>
                          <h4 class="text-uppercase">Vector Illustrator</h4>                          
                        </div>
                        <div class="col-specialization col-md-6 col-lg-4">
                          <span class="icon-specialization fal fa-pencil-paintbrush"></span>
                          <h4 class="text-uppercase">UI / UX</h4>                          
                        </div>
                        <div class="col-specialization col-md-6 col-lg-4">
                          <span class="icon-specialization fal fa-phone-laptop"></span>
                          <h4 class="text-uppercase">Web Design</h4>
                        </div>
                      </div>
                      <!--<div class="view-all">
                        <a href="#">
                          Download resume
                        </a>
                      </div>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div data-anchor="page3" class="pp-scrollable text-white section section-3">
        <div class="scroll-wrap">
          <div class="section-bg mask" style="background-image:url(images/bg/resume.jpg);"></div>
          <div class="scrollable-content">
            <div class="vertical-title d-none d-lg-block"><span>resume</span></div>
            <div class="vertical-centred">
              <div class="boxed boxed-inner">
                <div class="boxed">
                  <div class="container">
                    <div class="intro">
                      <div class="row row-resume">
                        <div class="col-md-6 fadeY fadeY-1">
                          <h2 class="resume-header title"><i class="far fa-book-alt"></i> Education</h2>
                          <div class="col-resume rounded">
                            <div class="resume-content">
                              <div class="resume-inner">
                                <div class="resume-row">
                                  <h6 class="resume-type">Academy course</h6>
                                  <i class="resume-study">STMIK Mikroskil, Medan, Indonesia<br>Jun 2013 - Sep 2016</i>
                                  <p class="resume-text">Technical Information based in Programming and Web Design.</p>
                                </div>
                                <div class="resume-row">
                                  <h6 class="resume-type">Specialization course</h6>
                                  <i class="resume-study">Lancang Kuning University, Pekanbaru, Indonesia<br>Oct 2016 - Jul 2018</i>
                                  <p class="resume-text">The Faculty of Engineering is based on Architectural Engineering</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 pt-md-5 mt-md-5 fadeY fadeY-2">
                          <h2 class="resume-header title"><i class="far fa-telescope"></i> Experience</h2>
                          <div class="col-resume rounded">
                            <div class="resume-content">
                              <div class="resume-inner">
                                <div class="resume-row">
                                  <h6 class="resume-type">WEBDESIGNER &amp; PRINTING</h6>
                                  <i class="resume-study">YLIRA Foundation<br>Jan 2014 - Nov 2016</i>
                                  <p class="resume-text">Specializing web web design and printing for T-Shirt, Brochure, Business card, Invitation.</p>
									<h6 class="resume-type">Logo Creator</h6>
								<ul>
								<li>
									<i class="resume-study">Youth Association, SEI TUAN - Sep 2015</i>
								</li>
									<li><i class="resume-study">Ruma KERJA, Pekanbaru - Sep 2015</i>
									</li>
									<li><i class="resume-study">Student Council SMAN 1, Tualang - Sep 2015</i>
									</li>
								</ul>
                                </div>
                                <div class="resume-row">
                                  <h6 class="resume-type">GRAPHIC DESIGNER</h6>
									<ul>
										<li class="resume-study">Ruma KERJA, Pekanbaru | Sep 2017 - now</li>
										<li class="resume-study">REMAJA HKBP, Tualang | Mar 2016 - now </li>
										<li class="resume-study">MMM Craft, Pekanbaru | Okt 2016 - Sep 2017 </li>
									</ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                       </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div data-anchor="page4" class="pp-scrollable section section-4">
        <div class="scroll-wrap">
          <div class="section-bg bg-about" style="background-image:url(images/bg/about.jpg);"></div>
          <div class="scrollable-content">
            <div class="vertical-title text-white  d-none d-lg-block"><span>about me</span></div>
            <div class="vertical-centred">
              <div class="boxed boxed-inner">
                <div class="boxed">
                  <div class="container">
                    <div class="intro">
                      <div class="row align-items-center">
                        <div class="col-lg-6 offset-xl-1">
                          <div class="experience-box">
                            <div class="experience-content">
                              <div class="experience-number">4</div>
                              <div class="experience-info"><div>Years<br>Experience<br>Working</div></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5 mt-5 mt-xl-0">
                          <h2 class="title"><span class="text-primary"><i class="fad fa-cogs"></i> Software</span> Experience</h2>
                          <div class="progress-bars">
                            <div class="clearfix">
                              <div class="number float-left">Adobe Illustrator</div>
                              <div class="number float-right">95%</div>
                            </div>
                            <div class="progress">
                              <div class="progress-bar-wrp">
                                <div class="progress-bar" role="progressbar" style="width: 95%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                            </div>
                            <div class="clearfix">
                              <div class="number float-left">Adobe Photoshop</div>
                              <div class="number float-right">75%</div>
                            </div>
                            <div class="progress">
                              <div class="progress-bar-wrp">
                                <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                            </div>
                            <div class="clearfix">
                              <div class="number float-left">Adobe Indesign</div>
                              <div class="number float-right">70%</div>
                            </div>
                            <div class="progress">
                              <div class="progress-bar-wrp">
                                <div class="progress-bar" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                            </div>
							  <div class="clearfix">
                              <div class="number float-left">Affinity Designer</div>
                              <div class="number float-right">85%</div>
                            </div>
                            <div class="progress">
                              <div class="progress-bar-wrp">
                                <div class="progress-bar" role="progressbar" style="width: 85%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
		
      <div data-anchor="page5" class="pp-scrollable text-white section section-5">
        <div class="scroll-wrap">
          <div class="bg-changer">
            <div class="section-bg" style="background-image:url(images/bg/project1.jpg);"></div>
            <div class="section-bg" style="background-image:url(images/bg/project2.jpg);"></div>
            <div class="section-bg" style="background-image:url(images/bg/project3.jpg);"></div>
            <div class="section-bg" style="background-image:url(images/bg/project4.jpg);"></div>
			<div class="section-bg" style="background-image:url(images/bg/project5.jpg);"></div>
          </div>
          <div class="scrollable-content">
            <div class="vertical-title  d-none d-lg-block"><span>Gallery</span></div>
            <div class="vertical-centred">
              <div class="boxed boxed-inner">
                <div class="boxed">
                  <div class="container">
                    <div class="intro">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="project-row">
                            <a class="active" href="https://www.behance.net/gallery/96919555/Isometric-Time-Management-Illustration" target="_blank">
                              <span class="project-number">01</span>
                              <h2 class="project-title">Time Management</h2>
                              <div class="project-category">Isometric Illustration</div>
                            </a>
                          </div>
                          <div class="project-row">
                            <a href="https://www.behance.net/gallery/95542655/Isometric-E-Learning-Activity" target="_blank">
                              <span class="project-number">02</span>
                              <h2 class="project-title">E-Learning Activity</h2>
                              <div class="project-category">Isometric Illustration</div>
                            </a>
                          </div>
                          <div class="project-row">
                            <a href="https://www.behance.net/gallery/97565655/Business-Management-Illustration-Concept" target="_blank">
                              <span class="project-number">03</span>
                              <h2 class="project-title">Business Management</h2>
                              <div class="project-category">Isometric Illustration</div>
                            </a>
                          </div>
                          <div class="project-row">
                            <a href="https://www.behance.net/gallery/97620173/Isometric-E-Learning-Activity-v2" target="_blank">
                              <span class="project-number">04</span>
                              <h2 class="project-title">E-Learning Activity V2</h2>
                              <div class="project-category">Isometric Illustration</div>
                            </a>
                          </div>
							<div class="project-row">
                            <a href="https://www.behance.net/gallery/97874019/Business-Management-Illustration-Concept-V2" target="_blank">
                              <span class="project-number">05</span>
                              <h2 class="project-title">Business Management V2</h2>
                              <div class="project-category">Isometric Illustration</div>
                            </a>
                          	</div>
                          <div class="view-all view-all-projects">
                            <a  href="https://Behance.net/HendricSimarmata" target="_blank">
                              View all projects on Behance
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

<div data-anchor="page6" class="pp-scrollable section section-6">
        <div class="scroll-wrap">
          <div class="scrollable-content">
            <div class="vertical-title text-white d-none d-lg-block"><span>my gallery</span></div>
            <div class="vertical-centred">
              <div class="boxed boxed-inner">
                <div class="boxed">
                  <div class="container">
                    <div class="intro">
                       <h2 class="title text-white"><span class="text-primary">My</span> Gallery</h2>
                      <div class="row row-partners" id="image-popups">
                        <div class="col-lg-3 col-md-4 col-6 col-partner">
							
                          <div class="partner-inner"><a href="images/gallery/g1.png" data-effect="mfp-zoom-in"><img class="" alt="Isometric E-Learning Activity V1" title="Isometric E-Learning Activity V1" src="images/gallery/g1.png"></a></div>
							
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner" data-effect="mfp-zoom"><a href="images/gallery/g2.png" data-effect="mfp-zoom-in"><img alt="Isometric Business Management" title="Isometric Business Management" src="images/gallery/g2.png"></a></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner"  data-effect="mfp-zoom"><a href="images/gallery/g3.png" data-effect="mfp-zoom-in"><img alt="Isometric E-Learning Activity V2" title="Isometric E-Learning Activity V2" src="images/gallery/g3.png"></a></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner"  data-effect="mfp-zoom"><a href="images/gallery/g4.png" data-effect="mfp-zoom-in"><img alt="Home Activity" title="Home Activity" src="images/gallery/g4.png"></a></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner"  data-effect="mfp-zoom"><a href="images/gallery/g5.png" data-effect="mfp-zoom-in"><img alt="Isometric Research Illustration" title="Isometric Research Illustration" src="images/gallery/g5.png"></a></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner"  data-effect="mfp-zoom"><a href="images/gallery/g6.png" data-effect="mfp-zoom-in"><img alt="Isometric Office Activity" title="Isometric Office Activity" src="images/gallery/g6.png"></a></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner"  data-effect="mfp-zoom"><a href="images/gallery/g7.png" data-effect="mfp-zoom-in"><img alt="Onlien Learning - Landing Page" title="Onlien Learning - Landing Page" src="images/gallery/g7.png"></a></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner"  data-effect="mfp-zoom"><a href="images/gallery/g8.png" data-effect="mfp-zoom-in"><img alt="Work From Home Landing Page Illustration" title="Work From Home Landing Page Illustration" src="images/gallery/g8.png"></a></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner"  data-effect="mfp-zoom"><a href="images/gallery/g9.png" data-effect="mfp-zoom-in"><img alt="Isometric Time Management" title="Isometric Time Management" src="images/gallery/g9.png"></a></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner"  data-effect="mfp-zoom"><a href="images/gallery/g10.png" data-effect="mfp-zoom-in"><img alt="Sport Activity - Landing Page" title="Sport Activity - Landing Page" src="images/gallery/g10.png"></a></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner"  data-effect="mfp-zoom"><a href="images/gallery/g11.png" data-effect="mfp-zoom-in"><img alt="Outdoor Activity - Landing Page" title="Outdoor Activity - Landing Page" src="images/gallery/g11.png"></a></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-3 col-partner">
                          <div class="partner-inner"  data-effect="mfp-zoom"><a href="images/gallery/g12.png" data-effect="mfp-zoom-in"><img alt="Isometric E-Book Illustration - Landing Page" title="Isometric E-Book Illustration - Landing Page" src="images/gallery/g12.png"></a></div>
                        </div>
                      </div>
					<div class="row row-partners p-4 mt-5">
						<div class="col text-center">
							<i class="text-muted">Visit my Behance for more Gallery</i><br>
							<a type="button" role="button" href="https://behance.net/HendricSimarmata" target="_blank" class="btn behance btn-outline-light rounded py-3 px-5 text-center align-content-center"><i class="fab fa-behance "></i></a>
						</div>
					</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <div data-anchor="page7" class="pp-scrollable text-white section section-7">
        <div class="scroll-wrap">
          <div class="section-bg" style="background-image:url(images/bg/reviews.jpg);"></div>
          <div class="bg-quote"></div>
          <div class="scrollable-content">
            <div class="vertical-title  d-none d-lg-block"><span>testimonials</span></div>
            <div class="vertical-centred">
              <div class="boxed boxed-inner">
                <div class="boxed">
                  <div class="container">
                    <div class="intro">
                      <div class="review-carousel owl-carousel">
                        <div class="review-carousel-item">
                          <div class="review-row">
                            <div class="row">
                              <div class="col-md-5">
                                <div class="review-author">
                                  <div class="author-name">David & Elisa</div>
                                  <i>Apartment view lake at Brooklyn</i>
                                </div>
                              </div>
                              <div class="col-md-7 text">
                              <p>If you are seeking an Interior designer that will understand exactly your needs, and someone who will utilise their creative and technical skills in parity with your taste, then Suzanne at The Ramsay Studio is perfect.</p>
                              </div>
                            </div>
                          </div>
                          <div class="review-row">
                            <div class="row">
                              <div class="col-md-5">
                                <div class="review-author">
                                  <div class="author-name">Amanda</div>
                                  <i>Apartment view lake at Brooklyn</i>
                                </div>
                              </div>
                              <div class="col-md-7 text">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio dolorem reiciendis doloremque veniam perspiciatis quam velit pariatur eius, repellendus dolores eveniet maiores sed. Quod quam minus dolore sed cumque aliquid.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="review-carousel-item">
                          <div class="review-row">
                            <div class="row">
                              <div class="col-md-5">
                                <div class="review-author">
                                  <div class="author-name">David & Elisa</div>
                                  <i>Apartment view lake at Brooklyn</i>
                                </div>
                              </div>
                              <div class="col-md-7 text">
                              <p>If you are seeking an Interior designer that will understand exactly your needs, and someone who will utilise their creative and technical skills in parity with your taste, then Suzanne at The Ramsay Studio is perfect.</p>
                              </div>
                            </div>
                          </div>
                          <div class="review-row">
                            <div class="row">
                              <div class="col-md-5">
                                <div class="review-author">
                                  <div class="author-name">Amanda</div>
                                  <i>Apartment view lake at Brooklyn</i>
                                </div>
                              </div>
                              <div class="col-md-7 text">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio dolorem reiciendis doloremque veniam perspiciatis quam velit pariatur eius, repellendus dolores eveniet maiores sed. Quod quam minus dolore sed cumque aliquid.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="review-carousel-item">
                          <div class="review-row">
                            <div class="row">
                              <div class="col-md-5">
                                <div class="review-author">
                                  <div class="author-name">David & Elisa</div>
                                  <i>Apartment view lake at Brooklyn</i>
                                </div>
                              </div>
                              <div class="col-md-7 text">
                              <p>If you are seeking an Interior designer that will understand exactly your needs, and someone who will utilise their creative and technical skills in parity with your taste, then Suzanne at The Ramsay Studio is perfect.</p>
                              </div>
                            </div>
                          </div>
                          <div class="review-row">
                            <div class="row">
                              <div class="col-md-5">
                                <div class="review-author">
                                  <div class="author-name">Amanda</div>
                                  <i>Apartment view lake at Brooklyn</i>
                                </div>
                              </div>
                              <div class="col-md-7 text">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio dolorem reiciendis doloremque veniam perspiciatis quam velit pariatur eius, repellendus dolores eveniet maiores sed. Quod quam minus dolore sed cumque aliquid.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <div data-anchor="page8" class="pp-scrollable section section-8">
        <div class="scroll-wrap">
          <div class="section-bg" style="background-image:url(images/bg/contact.jpg);"></div>
          <div class="scrollable-content">
            <div class="vertical-title text-white d-none d-lg-block"><span>contact</span></div>
            <div class="vertical-centred">
              <div class="boxed boxed-inner">
                <div class="boxed">
                  <div class="container">
                    <div class="intro overflow-hidden">
                      <div class="row">
                        <div class="col-md-6">
                          <h2 class="title"><span class="text-primary">Prabumulih</span>, Indonesia</h2>
                          <h6 class="text-muted">I do my best to respond to your E-MAILS as soon as possible. Thank You!</h6>
                          <section class="contact-address m-0">
							<h4>Email</h4>
                            <h5><a class="mail" href="mailto:contact@hendricsimarmata.com"> contact@hendricsimarmata.com</a></h5>
                          </section>
                        </div>
                        <div class="col-md-6" id="form4-2">
                          <div data-anchor="contactform" class="contact-info" data-form-type="formoid">
                            <form class="js-form text-primary" action="https://mobirise.com/" method="POST" data-form-title="Mobirise Form">
							<input type="hidden" name="email" data-form-email="true" value="eNhd6Yxu2Z5FF3ngDa0rKXFZd7QgI+zF4QZH4+zcMaSTGWNDlv8RiqSorRaej8skSZuY2o+OCu4hw6Nv8aIdpQk+ymMZcHFlzmpujtv7HR9Kkrg849qYtkTPpCYrW//r">
								<div class="row">
									<div class="form-group col-sm-6" data-for="name">
                                  		<input type="text" class="rounded" name="name" placeholder="Name*" data-form-field="Name" required="required" id="name-form4-2" aria-required="true">
                                	</div>
									<div class="form-group col-sm-6" data-for="email">
                                  		<input type="text" class="rounded" name="email" placeholder="Email*" data-form-field="Email" required="required" id="email-form4-2" aria-required="true">
                                	</div>
								</div>
								<div class="row">
									<div class="form-group col-sm-12" data-for="subject">
                                  		<input type="text" class="rounded" name="subject" placeholder="Subject (Optinal)" data-form-field="Subject" required="required" id="subject-form4-2">
                                	</div>
								</div>
								<div class="row">
									<div class="form-group col-sm-12" data-for="message">
                                  		<textarea name="message" class="rounded" placeholder="Message*" data-form-field="Message" id="message-form4-2" aria-required="true"></textarea>
                                	</div>
								</div>
								<div class="row">
									<div hidden="hidden"  data-form-alert="" class="ml-3 mt-0 alert alert-success">Thank You, your message is successfully sent!</div>
									<div hidden="hidden" data-form-alert-danger="" class="ml-3 text-primary">Sorry, something went wrong</div>
								</div>
                                <div class="col-sm-12 p-0"><button type="submit" data-placement="top" title="Submit" class="button btn rounded">Contact me</button></div>
							  </form>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- jQuery -->

<script src="js/jquery/jquery-3.5.1.min.js"></script>
<script src="js/jquery/jquery-migrate-3.3.0.min.js"></script>
<script src="js/jquery/popper.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/animsition.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.pagepiling.min.js"></script>
<script src="js/fontawesome/fontawesome.js" data-auto-replace-svg="nest"></script>
<script src="js/lightbox.js"></script>

<!-- Scripts -->
<script src="js/scripts.js"></script>
<script src="js/formoid.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.js"></script>

</body>
</html>
